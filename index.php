<?php

use \mywishlist\models\Liste;
use \mywishlist\models\Item;
use \Illuminate\Database\Capsule\Manager as DB;

define('LISTES', 1);
define('LISTE_SOUHAIT', 2);
define('ITEM', 3);
define('ACCUEIL',4);
define('ERR',5);

require_once 'vendor/autoload.php';


// connection base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

// appel vue Accueil
$app->get('/', function () {
	$listes = \mywishlist\models\Liste::all()->toArray();
	$vue = new \mywishlist\vue\VueAccueil();
	$vue->render(ACCUEIL);
});

// appel VueConnexion
$app->get('/connexion', function () {
	$vue = new \mywishlist\vue\VueConnexion();
	$select = null;
	$vue->render($select);
});

// appel controleur connexion
$app->post('/connexion', function () {
	$c = new \mywishlist\controler\ControleurConnexion();
	$c->verification_utilisateur();
});


// vue deconnexion
$app->get('/deconnexion', function () {
	$c = new \mywishlist\controler\ControleurDeconnexion();
	$c->deconnexion();
});


// vue profil
$app->get('/profil', function () {
	$vue = new \mywishlist\vue\VueProfil();
	$vue->render();
});


// vue creation liste
$app->get('/newliste', function () {
	$vue = new \mywishlist\vue\VueCreateur();
	$html ="";
	$vue->render($html);
});

// controleur creation
$app->post('/newliste', function () {
	$c = new \mywishlist\controler\ControleurCreateur();
	$c->creer_liste();
});



//appel VueInscription
$app->get('/inscription', function () {
	$vue = new \mywishlist\vue\VueInscription();
	$erreur = null;
	$vue->render($erreur);
});

// appel controleur inscription
$app->post('/inscription', function () {
	$c = new \mywishlist\controler\ControleurInscription();
	$c->createUser();
});




// affichage de toutes les listes
$app->get('/listes', function () {
	$listes = \mywishlist\models\Liste::all()->toArray();
	$vue = new \mywishlist\vue\VueParticipant([$listes]);
	$vue->render(LISTES);
});

// afficher une liste
$app->get('/liste/:no', function ($no) {
	if(null !== \mywishlist\models\Liste::find($no)){
		$liste_id = $no;
		$liste = \mywishlist\models\Liste::find($no)->toArray();
		$items = \mywishlist\models\Item::select( '*')
																			->where('liste_id', '=', $liste_id)
																			->get();

		$vue = new \mywishlist\vue\VueParticipant([$liste]);
		$vue->ajouter_items([$items]);
		$vue->render(LISTE_SOUHAIT);
	}else{
		$vue = new \mywishlist\vue\VueAccueil();
		$vue->render(ERR);
	}
});

// afficher 1 item
$app->get('/item/:id', function ($id) {
	if(null !== \mywishlist\models\Item::find($id)){
		$item = \mywishlist\models\Item::find($id)->toArray();
		$vue = new \mywishlist\vue\VueParticipant([$item]);
		$vue->render(ITEM);
	}else{
		$vue = new \mywishlist\vue\VueAccueil();
		$vue->render(ERR);
	}
});

// appel controleur reservation
$app->post('/liste/:no', function () {
	$c = new \mywishlist\controler\ControleurReservation();
	$c->reserver();
});

// appel controleur ajout
$app->post('/liste/:no', function () {
	$c = new \mywishlist\controler\ControleurAjout();
	$c->ajouter();
});

$app->run();
