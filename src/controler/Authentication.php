<?php


namespace mywishlist\controler;
use \mywishlist\models\Utilisateur;

session_start();

class Authentication{


  public static function authenticate ($email, $mdp) {
    $res = false;
    // charger utilisateur $emailser
    // vérifier $emailser->hash == hash(password)
    // charger profil ($emailser->id)
    $util = Utilisateur::where('email', '=', $email)->first();
    // on vérifie si l'email du l'utilisateur éxiste
      if(isset($util->email)){
        // on vérifie si l'émail et le mdp correspondent

        if(password_verify($mdp, $util->password)){
          // si oui on charge le profil et la variable de session
          Authentication::loadProfile($email);
          $res = true;
        }else{
          $_SESSION = [];
        }
      }
      return($res);
    }


  private static function loadProfile($email) {
    // détruire la variable de session
    $_SESSION['email'] = array();
    // charger variable de session
    $_SESSION = ['email' => $email];
    //$_SESSION['email'] = $email;
  }

  public static function deconnexion(){
    $_SESSION = array();
    session_destroy();
  }


  public static function checkAccessRights ( $required ) {
        $required_level = ADMIN_LEVEL;
        // vérifier si variable session existe
        if(isset(($_SESSION['utilisateur']))){
          // verifiier droits de la variable
          if($_SESSION[ 'utilisateur'][ 'auth_level' ] >= $required_level){
            //throw new AuthException ;
            if(Authentication::checkAccessRights($required_level)){

            }else{
              throw new \Exception("Droit refuse");
            }
          }
        }
  }

  public static function createUser ($mail,$mdp, $nom, $prenom) {
    // vérifier la conformité de password avec la police
    // si ok : hacher password
    $hash = password_hash($mdp,PASSWORD_DEFAULT, ['cost'=>12]);
    $emailtil = new Utilisateur();
    $emailtil->email = $mail;
    $emailtil->password = $hash;
    $emailtil->nom_utilisateur = $nom;
    $emailtil->prenom_utilisateur = $prenom;
    $emailtil->save();
    self::loadProfile($mail);
  }




  public static function get_user(){
    if(isset($_SESSION['email'])){
      $email = $_SESSION['email'];
      $util = Utilisateur::find($email);
      return($util);
    }else{
      $erreur = "deconnecté";
      return($erreur);
    }

  }

}
