<?php

namespace mywishlist\controler;
use \mywishlist\models\Utilisateur;
use \mywishlist\vue\VueProfil;
use \Illuminate\Database\Capsule\Manager as DB;

require_once 'vendor/autoload.php';

// connection base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

session_start();


class ControleurConnexion{

  public function verification_utilisateur(){

        //vérification des informations rentrées
        if(isset($_POST['formconnexion'])){
          // vérification champs non vides
          if(!empty($_POST['emailconnect']) AND !empty($_POST['passwordconnect'])){
            // récupération données rentrés dans le formulaire connexion
            $email = htmlspecialchars($_POST['emailconnect']);
            //$mdp = password_hash($_POST['passwordconnect'],PASSWORD_DEFAULT, ['cost'=>12]);
            $mdp = $_POST['passwordconnect'];
            // on appel la fonction d'Authentication
            $res = Authentication::authenticate($email, $mdp);

            if($res){
              Authentication::authenticate($email, $mdp);
              $vue = new VueProfil();
              $vue->render();
            }else{
              $erreur = "Email ou mot de passe incorrect !";
            }
          }else{
            $erreur = "Tous les champs doivent êtres complétés !";
          }
        }

        if(isset($erreur)){
          $vue = new \mywishlist\vue\VueConnexion();
        	$vue->render($erreur);
        }
    }



  }
