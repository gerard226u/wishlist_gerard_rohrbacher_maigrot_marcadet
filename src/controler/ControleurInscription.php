<?php

namespace mywishlist\controler;
use \mywishlist\models\Utilisateur;
use \mywishlist\vue\VueProfil;
use \Illuminate\Database\Capsule\Manager as DB;

require_once 'vendor/autoload.php';

// connection base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

define('ERR_1', 1);
define('ERR_2', 2);
define('ERR_3', 3);
define('ERR_4', 4);
define('ERR_5', 5);

session_start();

class ControleurInscription{



  // fonction creation utilisateur
  public function createUser(){


      // récupération des variables du formulaire
      $nom = $_POST['nom'];
      $prenom = $_POST['prenom'];
      $mail = $_POST['email'];
      $mdp = $_POST['password'];
      $mdp2 = $_POST['password2'];

      //vérification des informations rentrées
      if(isset($_POST['forminscription'])){
        // on vérifie que tous les champs sont remplis
        if(!empty($_POST['nom']) AND !empty($_POST['prenom']) AND !empty($_POST['email']) AND !empty($_POST['password']) AND !empty($_POST['password2'])){
          $nom = htmlspecialchars($_POST['nom']);
          $prenom = htmlspecialchars($_POST['prenom']);
          $mail = htmlspecialchars($_POST['email']);
          $mdplength = strlen($mdp);
          // vérifier si l'email existe deja
          $exist = Utilisateur::where('email', '=', $mail)->first();
          if(!isset($exist)){
            // on vérifie le format de l'email
            if(filter_var($mail, FILTER_VALIDATE_EMAIL)){
              // on vérifie la longueur des mots de passe
              if($mdplength >= 6){
                // on vérifie que les mdp sont identiques
                if($mdp == $mdp2){
                  // si tout est vérifié on creer l'utilisateur
                  Authentication::createUser($mail,$mdp, $nom, $prenom);
                  $vue = new VueProfil();
                  $vue->render();
                }else{
                  $erreur = "Les mots de passes ne sont pas identiques !";
                }
              }else{
                $erreur = "Le mot de passe doit faire plus de 7 caractères !";
              }
            }else{
              $erreur = "L'adresse mail est invalide !";
            }
          }else{
            $erreur = "L'email est deja utilisé !";
          }
        }else{
          $erreur = "Tous les champs doivent être complétés !";
        }
      }
      if(isset($erreur)){
        $vue = new \mywishlist\vue\VueInscription();
      	$vue->render($erreur);
      }


    }


}
