<?php

namespace mywishlist\controler;
use \mywishlist\models\Liste;
use \mywishlist\models\Utilisateur;
use \mywishlist\vue\VueProfil;
use \Illuminate\Database\Capsule\Manager as DB;

require_once 'vendor/autoload.php';

// connection base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

session_start();

class ControleurCreateur{

  public function creer_liste(){

    // on vérifie que les champs sont bien remplis
    if(  !empty($_POST['titre']) AND !empty($_POST['desc'])  AND !empty($_POST['date'])){
          if(isset($_SESSION['email'])){
            $email = $_SESSION['email'];
            // récupération des variables du formulaire
            $titre = htmlspecialchars($_POST['titre']);
            $desc = htmlspecialchars($_POST['desc']);
            $expi = $_POST['date'];
            $user = \mywishlist\models\Utilisateur::where($email);
                  if(isset($user)){

                    $user_id = \mywishlist\models\Utilisateur::select( 'id_utilisateur')
                                    ->where( 'email', 'like', $email)
                                    ->first() ;
                    if(isset($user_id)){
                      echo "";
                    }

                    preg_match_all('#[0-9]+#',$user_id,$extract);
                    $nombre = $extract[0][0];

                    $liste = new Liste();
                    $liste->user_id = $nombre;
                    $liste->titre = $titre;
                    $liste->description = $desc;
                    $liste->expiration = $expi;
                    $liste->token = "token";
                    $liste->save();

                    // creer vue liste / num créé
                    $html = '<p>Votre liste a bien été créé !</p>';
                    //\mywishlist\controler\Authentication::deconnexion();
                    $vue = new \mywishlist\vue\VueAccueil();
                  	$vue->render(LISTE);
                }else{
                  echo "erreur1";
                }
          }else{
            echo "variable session inexistante";
          }



    }
  }



}
