<?php

namespace mywishlist\controler;
namespace mywishlist\vue;
use \mywishlist\models\Utilisateur;
use \Illuminate\Database\Capsule\Manager as DB;

require_once 'vendor/autoload.php';

session_start();

// connection base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

class VueProfil{

	public function afficher_null(){
		$html = '
				<p>
					Connectez vous pour voir votre profil !
				</p>

		';
		return($html);
	}

	public function afficher_Info(){
		$mail = $_SESSION['email'];
		$util = Utilisateur::where('email', '=', $mail)->first();
		$html = '<p>
					Votre profil :
				</p>' .
				'<p>' . 'Email : ' . $_SESSION['email'] . '</p>' . '<br>' .
				'<p>' . 'Nom : ' . $util->nom_utilisateur . '</p>'  . '<br>' .
				'<p>' . 'Prenom : ' . $util->prenom_utilisateur . '</p>'  . '<br>';
		return($html);
	}

	// methode affichage general
	public function render(){

		if(isset($_SESSION['email'])){
			$html = $this->afficher_Info();
		}else{
			$html = $this->afficher_null();
		}

	$html = <<<END
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="../web/css/accueil.css" />
		<link rel="shortcut icon" href="../web/img/logo.ico">
		<title>My WishList</title>

		<div class="header">
		</div>

			<nav>
					<ul>
					<div class="topnav">
						<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php">Accueil</a></li>
						<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/connexion">Connexion</a></li>
						<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/inscription">Inscription</a></li>
						<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/profil">Mon profil</a></li>
						<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/newliste">Creer une liste</a></li>
					</div>
					</ul>
			</nav>
	</head>
    <body>

		<article>
			<div class = "content">
				$html
			</div>
		</artile>

			<p>
				<a href ="#">Editer mon profil</a>
				<a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/deconnexion">Deconnexion</a>
			</p>

	  <footer>
	  </footer>

	</body>
	</html>

END;

	echo $html;
	}

}
