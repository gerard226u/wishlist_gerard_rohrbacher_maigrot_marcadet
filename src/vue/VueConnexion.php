<?php
namespace mywishlist\vue;

session_start();

class VueConnexion{

	public function affichage_connexion($select){
		$html =
		'<form action="" method="POST">

				<p class = "p_form">
					Connectez-vous pour pouvoir accéder à vos listes de souhait :
				</p>

				<div>
						<label>Login (email) :</label>
						<input type="email" placeholder="Votre email" id="nom" name="emailconnect" />
				</div>
				<div>
						<label for="password">Mot de passe :</label>
						<input type="password" placeholder="Votre mot de passe" id="password" name="passwordconnect" />
				</div>

				<div class="button">
						<button class="button" name = "formconnexion">connexion</button>
				</div>

				<div class="content">
						' . $select .  '
				</div>

		</form>';
		return($html);
	}

	public function affichage_connecté(){
		$html = '<article>
			<p>Vous êtes deja connecté ! </br></p>

			<a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/deconnexion">Deconnexion</a>
			</article>
		';
		return($html);
	}

	// methode affichage general
	public function render($select){

		if(isset($_SESSION['email'])){
			$content = $this->affichage_connecté();
		}else {
			$content = $this->affichage_connexion($select);
		}

	$html = <<<END
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="../web/css/connexion.css" />
		<link rel="shortcut icon" href="../web/img/logo.ico">
		<title>My WishList</title>

		<div class="header">
		</div>

			<nav>
					<ul>
					<div class="topnav">
						<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php">Accueil</a></li>
						<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/connexion">Connexion</a></li>
						<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/inscription">Inscription</a></li>
						<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/profil">Mon profil</a></li>
						<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/newliste">Creer une liste</a></li>
					</div>
					</ul>
			</nav>
	</head>
    <body>

			<P>
			  $content
			</p>

	  <footer>
	  </footer>

	</body>
	</html>

END;

	echo $html;
	}

}
