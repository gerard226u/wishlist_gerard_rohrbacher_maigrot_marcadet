<?php
namespace mywishlist\vue;

session_start();

class VueParticipant{


	protected $tab;
	protected $items;

	// constructeur qui reçoit un tableau d'item ou listes à afficher
	public function __construct(array $t){
		$this->tab = $t;
	}

	// constructeur qui reçoit une liste et ses items
	public function ajouter_items(array $i){
		$this->items = $i;
	}


	// affichage de toutes les listes
	private function affichageListes(){
		$html = '<link rel="stylesheet" type="text/css" href="../web/css/participant.css" />' . '<table id= "customers">' .
							'<tr>' .
								'<th>' . 'numero' . '</th>' .
								'<th>' . 'titre' . '</th>' .
								'<th>' . 'description' . '</th>' .
								'<th>' . 'expiration' . '</th>' .
							'</tr>';
		foreach($this->tab as $listes){
			foreach($listes as $liste){
				$html .= 	'<tr>' .
										'<td>' . $liste['no'] . '</td>' .
										'<td>' . $liste['titre'] . '</td>' .
										'<td>' . $liste['description'] . '</td>' .
										'<td>' . $liste['expiration'] . '</td>' .
									'</tr>';
			}
		}
		$html .=  '</table>';
		return $html;
	}


	// affichage pour une liste de souhait + ses items
	private function affichageListeSouhaits(){
		$liste_id = 0;
		// tableau pour afficher les info de la liste
		$html = '<table id= "customers">' .
							'<tr>' .
								'<th>' . 'nom' . '</th>' .
								'<th>' . 'description' . '</th>' .
								'<th>' . 'expiration' . '</th>' .
							'</tr>';
		foreach($this->tab as $liste){
			$no = $liste['no'];
				$html .= 	'<tr>' .
										'<td>' . $liste['titre'] . '</td>' .
										'<td>' . $liste['description'] . '</td>' .
										'<td>' . $liste['expiration'] . '</td>';
									'</tr>';
		}
		$html .=  '</table><br>';

// requete pour recuperer les items de la liste
		$items = \mywishlist\models\Item::select( '*')
																			->where('liste_id', '=', $no)
																			->get();

	// tableau avec tous les items de la liste
		$html .= '<table id= "customers">' .
							'<tr>' .
								'<th>' . 'id' . '</th>' .
								'<th>' . 'nom' . '</th>' .
								'<th>' . 'description' . '</th>' .
								'<th>' . 'tarif' . '</th>' .
								'<th>' . 'reservé' . '</th>' .
								'<th>' . 'image' . '</th>' .
							'</tr>';

		foreach($items as $item){
			// ../img/flou_wallpaper.jpg
			// $image = "/MyWishlist/web/img/" . $item['img'];
			$image = "../../web/img/" . $item['img'];
			$html .= '<tr>' .
									'<td>' . $item['id'] . '</td>' .
									'<td>' . $item['nom'] . '</td>' .
									'<td>' . $item['descr'] . '</td>' .
									'<td>' . $item['tarif'] . '€' . '</td>' .
									'<td>' . $item['reservation'] . '</td>' .
									'<td align="center">' . '<img src= ' . $image .  ' alt="titre" width="460" height="345"/>' .'</td>' .
								'</tr>';
			}
		$html .=  '</table><br>';

		return $html;
	}






	// affichage d'un item particulier
	public function affichageItem(){
		$html = '<link rel="stylesheet" type="text/css" href="../../web/css/participant.css" />' . '<table id= "customers">' .
							'<tr>' .
								'<th>' . 'id' . '</th>' .
								'<th>' . 'liste' . '</th>' .
								'<th>' . 'nom' . '</th>' .
								'<th>' . 'description' . '</th>' .
								'<th>' . 'tarif' . '</th>' .
								'<th>' . 'image' . '</th>' .
							'</tr>';
		foreach($this->tab as $item){
			$image = "/MyWishlist/web/img/" . $item['img'];
			$html .= '<tr>' .
									'<td>' . $item['id'] . '</td>' .
									'<td>' . $item['liste_id'] . '</td>' .
									'<td>' . $item['nom'] . '</td>' .
									'<td>' . $item['descr'] . '</td>' .
									'<td>' . $item['tarif'] . '</td>' .
									'<td align="center">' . '<img src= ' . $image .  ' alt="titre" width="460" height="345"/>' .'</td>' .
								'</tr>';
		}
		return $html;
	}




	private function affichage_reservation(){
		$res = '<form action="" method="POST">

				<p class = "p_form">
					Remplissez ce formulaire pour réserver un item maintenant :
				</p>

				<div>
						<label>Numero de l\'item :</label>
						<input type="text" id="num" name="num" />
				</div>

				<div>
						<label>Message pour le createur de la liste :</label>
						<textarea id="msg" name = "msg"></textarea>
				</div>

				<div class="button">
						<button class="button" name = "formconnexion">Resever l\'item</button>
				</div>

		</form>';

		return($res);
	}



	private function affichage_ajout(){
		$res = '<article>
			<form action="" method="POST">

				<p class = "p_form">
					Ajoutez un item a cette liste :
				</p>

				<div>
						<label>Numero de la liste :</label>
						<input type="number" id="num" name="num" />
				</div>

				<div>
						<label>Nom de l\'item :</label>
						<input type="text" id="nom" name="nom" />
				</div>

				<div>
						<label>Description de l\'item :</label>
						<textarea id="descr" name = "descr"></textarea>
				</div>

				<div>
						<label>Prix de l\'item :</label>
						<input type="number" id="prix" name="prix" />
				</div>

				<div class="button">
						<button class="button" name = "formconnexion">Ajouter l\'item</button>
				</div>

		</form> </article>';

		return($res);
	}







	// methode affichage general
	public function render($select){
		$form_reservation = "";
		$form_ajout = "";
		switch ($select){
			case LISTES:
				$content = $this->affichageListes();
				break;
			case LISTE_SOUHAIT:
				$content = $this->affichageListeSouhaits();
				if(isset($_SESSION['email'])){
					$form_reservation = $this->affichage_reservation();
					$form_ajout = $this->affichage_ajout();
				}else{
					$form_reservation = '<article><br>Connectez vous pour reserver ou ajouter un item maintenant !<br></article>';
				}
				break;
			case ITEM:
				$content = $this->affichageItem();
				break;
			case RESERV:
				$content = 'Votre item a bien été reservé !';
				break;
		}
	$html = <<<END
	<!DOCTYPE html>
	<html>
	<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="../../web/css/participant.css" />
	<link rel="shortcut icon" href="../../web/img/logo.ico">
	<title>My WishList</title>

	<div class="header">
	</div>

		<nav>
				<ul>
				<div class="topnav">
					<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php">Accueil</a></li>
					<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/connexion">Connexion</a></li>
					<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/inscription">Inscription</a></li>
					<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/profil">Mon profil</a></li>
					<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/newliste">Creer une liste</a></li>
				</div>
				</ul>
		</nav>
	</head>
	<body>

	<div class = "content">
		$content
	</div>


	$form_reservation
	$form_ajout

		<footer>

		</footer>

	</body>
	</html>

END;

	echo $html;
	}

}
