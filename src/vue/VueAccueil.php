<?php
namespace mywishlist\vue;

session_start();

class VueAccueil{

	private function affichage_Erreur(){
		$html = 'La page que vous demandez n\'éxiste pas !';
		return($html);
	}

	private function affichage_reserv(){
		$html = 'Votre item a bien été reservé !';
		return($html);
	}

	private function affichage_cree(){
		$html = 'Votre item a bien été ajouté !';
		return($html);
	}

	private function affichage_cree_liste(){
		$html = 'Votre liste a bien été ajouté !';
		return($html);
	}

	private function affichage_deja_reserv(){
		$html = 'L\'item est deja reservé !';
		return($html);
	}

	private function affichage_rempli(){
		$html = 'Le formulaire n\'est pas rempli !';
		return($html);
	}


  private function affichageAccueil(){
		$html = '<p>' . 'Ceci est la page d\'accueil du site de MyWishList !' . '<br>' .
		'Principes généraux  :' . '<br>' .
		'MyWishList.app est une application en ligne pour créer, partager et gérer des listes de cadeaux.
		L\'application permet à un utilisateur de créer une liste de souhaits à l\'occasion d\'un événement particulier (anniversaire, fin d\'année, mariage, retraite ...)
		et lui permet de diffuser cette liste de souhaits à un ensemble de personnes concernées. Ces personnes peuvent
		alors consulter cette liste et s\'engager à offrir 1 élément de la liste. Cet élément est alors marqué comme réservé dans cette liste.' . '<br>' . '</p>';
		return $html;
	}

	private function lien_1(){
		$html = '<link rel="stylesheet" type="text/css" href="web/css/accueil.css" />';
		return($html);
	}

	private function lien_2(){
		$html = '<link rel="stylesheet" type="text/css" href="../../web/css/accueil.css" />';
		return($html);
	}

	private function lien_3(){
		$html = '<link rel="stylesheet" type="text/css" href="../web/css/accueil.css" />';
		return($html);
	}


	// methode affichage general
	public function render($select){
		switch ($select){
				case ACCUEIL:
	          $content = $this->affichageAccueil();
						$lien = $this->lien_1();
					break;
				case ERR:
						$content = $this->affichage_Erreur();
						$lien = $this->lien_2();
					break;
				case RESERV:
						$content = $this->affichage_reserv();
						$lien = $this->lien_2();
					break;
				case CREE:
						$content = $this->affichage_cree();
						$lien = $this->lien_2();
					break;
				case LISTE:
				 		$content = $this->affichage_cree_liste();
						$lien = $this->lien_3();
					break;
					case DEJA_RESERV:
					 		$content = $this->affichage_deja_reserv();
							$lien = $this->lien_2();
						break;
						case REMPLI:
						 		$content = $this->affichage_rempli();
								$lien = $this->lien_2();
							break;
				default:
						$content = $select;
						$lien = $this->lien_2();
					break;
		}

	$html = <<<END
	<!DOCTYPE html>
	<html>
    <head>
			<meta charset="utf-8" />

			$lien
			<link rel="shortcut icon" href="web/img/logo.ico">
			<title>My WishList</title>


			<div class="header">
			</div>

				<nav>
						<ul>
							<div class="topnav">
								<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php">Accueil</a></li>
								<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/connexion">Connexion</a></li>
								<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/inscription">Inscription</a></li>
								<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/profil">Mon profil</a></li>
								<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/newliste">Creer une liste</a></li>
							</div>
						</ul>
		 			</nav>
    </head>

    <body>
				<article>
    			<div class = "content">
      			$content
    			</div>
				</artile>
		</body>

	<footer>

	</footer>

	</html>

END;

	echo $html;
	}

}
