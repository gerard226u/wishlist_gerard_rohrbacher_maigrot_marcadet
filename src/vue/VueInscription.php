<?php
namespace mywishlist\vue;

session_start();

class VueInscription{


	public function affichage_inscription($select){
		$html = '
		<form action="" method="post">

				<p class = "p_form">
					Inscivez-vous gratuitement pour accéder à toutes les fonctionnalités du site !
				</p>

				<div class="div_form">
						<label>Nom :</label>
						<input type="text" placeholder="Votre nom" name="nom" />
				</div>
				<div class="div_form">
						<label>Prenom :</label>
						<input type="text" placeholder="Votre prenom" name="prenom" />
				</div>

				<div class="div_form">
						<label>Email :</label>
						<input type="text" placeholder="Votre email" name="email" />
				</div>

				<div class="div_form">
						<label>Mot de passe :</label>
						<input type="password" placeholder="Votre mot de passe" name="password" />
				</div>

				<div class="div_form">
						<label>Confirmation du mot de passe :</label>
						<input type="password" placeholder="Votre mot de passe" name="password2" />
				</div class="div_form">

				<div class="button">
						<button class="button" name = "forminscription">Inscription</button>
				</div>

				<div class="content">
						' . $select . '
				</div>

		</form>
		';
		return($html);
	}

	public function affichage_inscrit(){
		$html = '<article>
			<p>Vous êtes deja inscrit ! </br></p>

			<a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/deconnexion">Deconnexion</a>
			</article>
		';
		return($html);
	}


	// methode affichage general
	public function render($select){
	$content = $select;

	if(isset($_SESSION['email'])){
		$content = $this->affichage_inscrit();
	}else {
		$content = $this->affichage_inscription($select);
	}

	$html = <<<END
	<!DOCTYPE html>
	<html>
    <head>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="../web/css/inscription.css" />
		<link rel="shortcut icon" href="../web/img/logo.ico">
		<title>My WishList</title>

			<div class="header">
			</div>

				<nav>
						<ul>
						<div class="topnav">
							<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php">Accueil</a></li>
							<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/connexion">Connexion</a></li>
							<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/inscription">Inscription</a></li>
							<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/profil">Mon profil</a></li>
							<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/newliste">Creer une liste</a></li>
						</div>
						</ul>
		 			</nav>
    </head>
    <body>

			<p>
				$content
			</p>

	  <footer>
	  </footer>

	</body>
	</html>

END;

	echo $html;
	}

}
