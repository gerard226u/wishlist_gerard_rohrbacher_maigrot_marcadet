<?php
namespace mywishlist\vue;

session_start();

class VueCreateur{


	public function affichage_connecté($res){
		$html = "";
		if(isset($_SESSION['email'])){
			$html = '
			<form id="f1" method="post">
						  ' . $res . '

						<div>
							<label for="titre">Titre de la liste :</label>
							<input type="text" id="titre" name="titre" />
						</div>

						<div>
							<label for="desc">Description de la liste de cadeaux :</label>
							<textarea id="desc" name = "desc"></textarea>
						</div>

						<div>
							<label for="date">Date d\'expiration :</label>
							<input type="date" id="date" name="date" />
						</div>

						<div>
							<button type="submit" name="valider" value="valid_reserv">Creer la liste</button>
						</div>

			</form>
			';
		}else {
			$html = 'Connectez-vous ou inscrivez-vous pour pouvoir creer une nouvelle liste !<br><br>';
		}
		return($html);
	}

	public function affichage_deco(){
		$html = 'Connectez-vous ou inscrivez-vous pour pouvoir creer une nouvelle liste ! <br> <br>';
		return($html);
	}



	// methode affichage general
	public function render($html){


	if(isset($_SESSION['email'])){
		$content = $this->affichage_connecté($html);
	}else {
		$content = $this->affichage_deco();
	}

	$html = <<<END
	<!DOCTYPE html>
		<html>
		<head>
			<meta charset="utf-8" />
			<link rel="stylesheet" type="text/css" href="../web/css/createur.css" />
			<link rel="shortcut icon" href="../web/img/logo.ico">
			<title>My WishList</title>

					<div class="header">
					</div>

						<nav>
								<ul>
								<div class="topnav">
									<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php">Accueil</a></li>
									<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/connexion">Connexion</a></li>
									<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/inscription">Inscription</a></li>
									<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/profil">Mon profil</a></li>
									<li><a href="https://webetu.iutnc.univ-lorraine.fr/~gerard226u/PHP/MyWishlist/index.php/newliste">Creer une liste</a></li>
								</div>
								</ul>
						</nav>
	  </head>
		<body>

		<article>
			<div class = "content">
				$content
			</div>
		</artile>

			<footer>
			</footer>

		</body>
		</html>

END;

	echo $html;
	}

}
